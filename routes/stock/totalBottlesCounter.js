module.exports = (db) => {
    const allLocationsCounter = (allStock) => {
        let bottleMap = {};
        let chartData = [];
    
        allStock.map((wine) => {
            if (bottleMap[wine.location] === undefined) {
                bottleMap[wine.location] = wine.totalBottels;
            } else {
                bottleMap[wine.location] += wine.totalBottels;
            };
        });
    
        for (item in bottleMap) {
            let wineItem = {name: item, value: bottleMap[item]};
    
            chartData.push(wineItem);
        };

        return chartData;
    };

    const locationsBottleCount = (req, res) => {
        db.Stock.find({}, { location: 1, totalBottels: 1 }, (err, data) => {
                if (err) res.status(500).json({
                    error: err,
                    status: 'Server Error Occurred'
                })
            })
            .then((docs) => {
                let result = allLocationsCounter(docs);
                res.status(200).json({
                    chartData: result,
                    message: "All Locations Bottle Count"
                })
            })
    };


    return locationsBottleCount;
}