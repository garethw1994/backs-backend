const mongoose = require('mongoose');

module.exports = (db) => {
    const bottleCounter = (packs) => {
        let bottleCount = 0;
        let packCount = 0;
        let boxesCount = 0;

        packs.map((wineItem) => {
            let type = wineItem.type.toLowerCase();
            let count = wineItem.count;

            if (type === 'bottles') {
                bottleCount = count;
            } else if (type === 'packs') {
                packCount = count * 3;
            } else {
                boxesCount = count * 6;
            };  
        });
        
        let bottleTotal = bottleCount + packCount + boxesCount;

        return bottleTotal;
    };
    
    const addStock = (req, res) => {
        let stock = JSON.parse(req.body.item)[0];
        // let totalBottels = bottleCounter(stock.packSize);

        let AddStock = new db.Stock({
            _id: new mongoose.Types.ObjectId(),
            code: stock.code,
            productImage: req.file.path,
            locations: stock.locations,
            name: stock.name,
            locationsCount: stock.locationCounts
        });

        // console.log(AddStock)
        AddStock.save((err, result) => {
            if (err) res.status(500).json({
                error: err,
                status: 'Server Error Occurred'
            })
            else
                res.status(200).json({
                    result: result,
                    status: 'Successfully Added'
                })
        });
    };

    return addStock
}