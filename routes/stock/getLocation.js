module.exports = (db) => {
    // filter for only wines under specific locations
    const filter = (wines, search) => {
        let listOfFilteredWines = [];

        for (var x = 0; x < wines.length; x++) {
            let currentLocations = wines[x].locations;
            for (var y = 0; y < currentLocations.length; y++) {
                if (currentLocations[y].toLowerCase() === search.toLowerCase()) {
                    listOfFilteredWines.push(wines[x]);
                    break;
                };
            };
        };

        return listOfFilteredWines;
    };

    const getLocation = (req, res) => {
        let searchLocation = req.params.location;

        if (searchLocation) {
            db.Stock.find({}, {
                    _id: 0
                }, (err) => {
                    if (err) res.status(500).json({
                        error: err,
                        status: 'Server Error Occurred'
                    })
                })
                .then(async (docs) => {
                    res.status(200).json({
                        status: 'Successfully Retrieved',
                        response: await filter(docs, searchLocation)
                    })
                })
        };
    };

    return getLocation;
}