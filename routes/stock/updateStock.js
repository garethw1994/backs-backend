module.exports = (db) => {
    const bottleCounter = (packs) => {
        let bottleCount = 0;
        let packCount = 0;
        let boxesCount = 0;
    
        packs.map((wineItem) => {
            let type = wineItem.type.toLowerCase();
            let count = wineItem.count;
    
            if (type === 'bottles') {
                bottleCount = count;
            } else if (type === 'packs') {
                packCount = count * 3;
            } else {
                boxesCount = count * 6;
            };  
        });
        
        let bottleTotal = bottleCount + packCount + boxesCount;
    
        return bottleTotal;
    };

    const updateStock = (req, res) => {
        let newStock = req.body[0].item;
        // let totalBottels = bottleCounter(newStock.packSize);

        db.Stock.updateOne({
                code: newStock.code
            }, {
                $set: {
                    locationsCount: newStock.locationsCount
                }
            }, (err, result) => {
                if (err) res.status(500).json({
                    error: err,
                    status: 'Server Error Occurred'
                })
            })
            .then((result) => {
                res.status(200).json({
                    status: 'Successfully Updated',
                    response: result
                })
            })
    };

    return updateStock;
}