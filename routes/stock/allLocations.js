module.exports = (db) => {
    const duplicationRemover = (list) => {
        let listMap = {};

            list.map((locationlist) => {
                locationlist.locations.map((location) => {
                    if (location.includes('Storage')) location = 'Storage Room';
                    
                    if (listMap[location] === undefined) {
                        listMap[location] = 1;
                    }
                })
            });

            let uniqueList = [];

            for (key in listMap) {
                uniqueList.push(key);
            };

            return uniqueList;
    };

    const allLocations = (req, res) => {
        db.Stock.find({}, {
                _id: 0,
                locations: 1
            }, (err, docs) => {
                if (err) res.status(500).json({
                    error: err,
                    status: 'Server Error Occurred'
                })
            })
            .then((locations) => {
                let uniqueLocations = duplicationRemover(locations);
                res.send(uniqueLocations);
            });
    };

    return allLocations;
}