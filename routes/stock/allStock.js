module.exports = (db) => {
    const allStock = (req, res) => {
        db.Stock.find({}, (err) => {
                if (err) {
                    res.status(500).json({
                        error: err,
                        status: 'Server Error Occurred'
                    })
                }
            })
            .then((docs) => {
                res.status(200)
                    .json({
                        status: 'Successfully Retrieved',
                        response: docs
                    })
            });
    };

    return allStock;
}