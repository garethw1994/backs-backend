// USER ROUTES
const addUser = require('./user/addUser');
const allUsers = require('./user/allUsers');
const loginUser = require('./user/loginUser');
const userDetails = require('./user/userDetails');
const removeUser = require('./user/removeUser');

// STOCK ROUTES
const addStock = require('./stock/addStock')
const allStock = require('./stock/allStock')
const getLocation = require('./stock/getLocation')
const updateStock = require('./stock/updateStock')
const totalBottlesCounter = require('./stock/totalBottlesCounter');
const allLocations = require('./stock/allLocations');

// EXCEL ROUTES
const createSpreadsheet = require('./excel/createWorkbook');
const fileList = require('./excel/excelFiles');
const downloadFile = require('./excel/downloadFile');
const deleteFile = require('./excel/deleteFile');

module.exports = (db) => {
    return {
        // USER ROUTES
        AddUser: addUser(db),
        LoginUser: loginUser(db),
        AllUsers: allUsers(db),
        UserDetails: userDetails(db),
        RemoveUser: removeUser(db),
        // STOCK ROUTES
        AddStock: addStock(db),
        AllStock: allStock(db),
        GetLocation: getLocation(db),
        UpdateStock: updateStock(db),
        AllLocations: allLocations(db),
        TotalBottles: totalBottlesCounter(db),
        // EXCEL ROUTES
        CreateSpreadsheet: createSpreadsheet(db),
        FileList: fileList(db),
        DownloadFile: downloadFile(db),
        DeleteFile: deleteFile(db)
    };
};