module.exports = (db) => {
    const deleteUser = (req, res) => {
        db.Users.remove({_id: req.body.id}, (err, result) => {
                if (err) res.status(500).json({
                    status: 'Server-side Error',
                    error: err
                });
            })
            .then((result) => {
                res.status(200).json({
                    message: 'Successfully removed user',
                    users: result
                })
            })
    };

    return deleteUser;
};