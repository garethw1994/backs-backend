const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (db) => {
    const login = (req, res) => {
        let user = req.body;

        try {
            db.Users.findOne({
                    username: user.username.toLowerCase()
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({
                            message: 'Unable To Find User',
                            error: error
                        })
                    }
                })
                .then(user => {
                    if (user === null) {
                        return res.status(401).json({
                            message: 'Incorrect Username or Password. Try Again.'
                        })
                    } else {
                        bcrypt.compare(req.body.password, user.password, (err, result) => {
                            if (err) {
                                return res.status(401).json({
                                    message: 'Auth Failed'
                                })
                            };

                            if (!result) {
                                return res.status(401).json({
                                    message: 'Incorrect Username or Password. Try Again.'
                                })
                            }

                            if (result) {
                                db.Users.updateOne({
                                        username: user.username
                                    }, {
                                        $set: {
                                            lastLogin: user.currentLogin,
                                            currentLogin: new Date()
                                        }
                                    })
                                    .then((result) => {
                                        const token = jwt.sign({
                                                username: user.username,
                                                role: user.role,
                                                lastLogin: user.currentLogin,
                                                currentLogin: new Date(),
                                                userID: user._id
                                            },
                                            "secret", {
                                                expiresIn: "1 hour"
                                            })
                                        return res.status(200).json({
                                            message: 'Auth successful',
                                            token: token
                                        })
                                    })
                            };
                        })
                    }


                })
        } catch (error) {
            return res.status(500).json({
                message: 'Authorization unsuccessful',
                error: error
            })
        }
    }

    return login
    
}