module.exports = (db) => {
    const allUsers = (req, res) => {
        db.Users.find({ username: { $ne: req.params.username } }, { username: 1, role: 1 }, (err, result) => {
                if (err) res.status(500).json({
                    status: 'Server-side Error',
                    error: err
                });
            })
            .then((users) => {
                res.status(200).json({
                    message: 'Successfully retrieved your contacts',
                    users: users
                })
            })
    };

    return allUsers;
};