const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

module.exports = (db) => {
    const addUser = (req, res) => {
        let credentials = req.body;
        // console.log(credentials)
        bcrypt.hash(credentials.password, 10, (err, hash) => {
            if (err) {
                return res.status(500).json({
                    error: err
                });
            } else {
                const user = new db.Users({
                    _id: new mongoose.Types.ObjectId(),
                    username: credentials.username.toLowerCase(),
                    role: credentials.role,
                    password: hash
                })

                user.save()
                    .then(result => {
                        res.status(201).json({
                            message: 'User created'
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            message: 'Error Occurred. Try Again',
                            error: err
                        })
                    })

            }
        })
    }

    return addUser
}