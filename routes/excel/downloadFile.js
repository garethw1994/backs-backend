const express = require('express');
const fs = require('fs');

module.exports = (db) => { 
    const downloadFile = async (req, res) => {
       let file = req.body.file;

       fs.exists(`build/excel/${file}`, (exists) => {
           if (exists)
            res.status(200)
                .json({
                    filePath: `build/excel/${file}`,
                    exists
                })
            else 
                res.status(403)
                    .json({
                        exists
                    })
       })
    };

    return downloadFile;
};