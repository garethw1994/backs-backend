const Excel = require('excel4node')

module.exports = CreateExcelFile = (workbook) => {
    return new Promise((resolve, reject) => {
        // create a excel file
        let date = Date.now();
        let newDate = Date(date).split(" ");

        let month = newDate[1];
        let day = newDate[2];
        let year = newDate[3];

        let filename = `StockTake_${day}-${month}-${year}.xlsx`;

        workbook.write(`build/excel/${filename}`, (error, stats) => {
            if (error) reject(error)
                else resolve(filename);
        })
    })
};
