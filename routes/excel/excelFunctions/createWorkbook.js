const Excel = require('excel4node')

module.exports = CreateWorkBook = () => {
    return new Promise((resolve, reject) => {
          try {
          // create a work book
          let workbook = new Excel.Workbook();
        
          resolve(workbook) 
        } catch (error) {
              reject(error);
        }
    })
};