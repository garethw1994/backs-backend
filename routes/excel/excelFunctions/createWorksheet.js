const Excel = require('excel4node');
const mongoose = require('mongoose');

module.exports = CreateWorkSheet = (workbook, db) => {
    return new Promise((resolve, reject) => {
        try {
            // create work sheet
            let worksheet = workbook.addWorksheet('Stock Count');

            // GET ALL DATA FUNC
            const getAllData = async () => {
                return await db.Stock.find({}, (err, docs) => {
                      if (err) return('Error Occurred.')
                        else return docs
                  });                
            };
     

            // MAP DATA
            getAllData()
                .then(async (data) => {
                    // map var
                    let wineMapper = {};

                    // increment through data
                    for (var i = 0; i < data.length; i++) {
                        let currentWine = data[i].name;
                        let currentLocations = data[i].locationsCount;

                        // increment over current wine's locations
                        for (var x = 0; x < currentLocations.length; x++) {
                            let currentPack = currentLocations[x].packSize[0];

                            let b = currentPack.type === 'bottles' ? currentPack.count : 0;
                            let y = currentPack.type === 'packs' ? currentPack.count : 0;
                            let n = currentPack.type === 'boxes' ? currentPack.count : 0;

                            // calcalate total bottles
                            // map total bottles to current wine
                            let totalBottles = b + (y * 3) + (n * 6);

                            if (wineMapper[currentWine] === undefined) {
                                wineMapper[currentWine] = totalBottles;
                            } else {
                                wineMapper[currentWine] += totalBottles;
                            };
                        };
                    };

                    // Add Column Headers
                    worksheet.cell(1, 1).string("Wine");
                    worksheet.cell(1, 2).string("Total Bottles");

                    // set counter
                    let counter = 2;

                    // Populate Worksheet with wines and total bottles
                    for (wine in wineMapper) {
                        worksheet.cell(counter, 1).string(wine);
                        worksheet.cell(counter, 2).number(wineMapper[wine]);

                        // inc counter
                        counter++;
                    };
                })
                .then(() => {
                    resolve(workbook);  
                })
                .catch((error) => {
                    throw new Error(error)
                })
        } catch (error) {
              reject(error);
        }
    })
};
  