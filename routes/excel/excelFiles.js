const express = require('express');
const fs = require('fs');

module.exports = (db) => { 
    const fileList = async (req, res) => {
        // read excel files
        fs.readdir('build/excel', (err, data) => {
            if (err)
                res.status(403)
                    .json({
                        files: [],
                        message: 'No Files Found'
                    })
            else 
                res.status(200).json({
                    message: 'successfully retrieved files',
                    files: data
                });
        })
    };

    return fileList;
};