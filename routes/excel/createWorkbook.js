const express = require('express');
const mongoose = require('mongoose');

const createwb = require('./excelFunctions/createWorkbook');
const createws = require('./excelFunctions/createWorksheet');
const createxlsxfile = require('./excelFunctions/createExcelFile');

module.exports = (db) => { 
    const createWorkbook = async (req, res) => {
        // create excel work book
        createwb()
            .then(workbook => {
                console.log('Created Work Book.');
                // create a new sheet
                createws(workbook, db)
                    .then((workbook) => {
                        console.log('Created Work Sheet');
                        // create a new excel file
                        createxlsxfile(workbook)
                            .then((fn) => {
                                console.log('Excel file create')
                                res.status(200)
                                    .json({
                                        path: `build/excel/${fn}`
                                    })
                            })
                             .catch((error) => {
                                 console.log('Excel File Creation');
                                 console.log(error);
                             })
                    })
                    .catch((error) => {
                        console.log('Worksheet Creation Process');
                        console.log(error);
                    })
            })
            .catch((error) => {
                console.log('Workbook Creation Process');
                console.log(error);
            });
    };

    return createWorkbook;
};