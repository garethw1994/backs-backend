const express = require('express');
const fs = require('fs');

module.exports = (db) => { 
    const deleteFile = async (req, res) => {
       let file = req.body.file;

       fs.unlink(`build/excel/${file}`, (error) => {
           if (error)
                res.status(500)
                    .json({
                        deleted: false
                    })
            else 
                res.status(200) 
                    .json({
                        deleted: true
                    })
       })
    };

    return deleteFile;
};