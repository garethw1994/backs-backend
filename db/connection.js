const mongoose = require('mongoose');

module.exports = (mongoURL) => {
    // Connect To DB
    mongoose.connect(mongoURL, {
        useNewUrlParser: true
    }, (err) => {
        if (!err) {
            console.log(`Connected To ${mongoURL} database`)
        } else {
            console.log(err)
        }
    });
};