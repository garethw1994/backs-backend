// prerequisites
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = () => {
    let UsersSchema = new Schema({
        _id: mongoose.Schema.Types.ObjectId,
        username: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        role: {
            type: String,
            required: true
        },
        lastLogin: {
            type: Date
        },
        currentLogin: {
            type: Date
        }
    });

    let StockSchema = new Schema({
        code: {
            type: String,
            required: true,
            max: 100
        },
        productImage: {
            type: String
        },
        locations: {
            type: Array,
            required: true,
            max: 100
        },
        name: {
            type: String,
            required: true,
            max: 100
        },
        locationsCount: {
            type: Array,
            required: true
        }
    });


    var Stock = mongoose.model('Stock', StockSchema);

    Stock.collection.createIndex({
        code: 1
    }, {
        unique: true
    });

    var Users = mongoose.model('Users', UsersSchema);

    Users.collection.createIndex({
        username: 1
    }, {
        unique: true
    });

    return {
        Stock,
        Users
    }
};