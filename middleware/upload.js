const multer = require('multer');

module.exports = () => {
     // Multer Image Save Destination
     const Storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'build/uploads/');
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname)
        }
    });

    const FileFilter = (req, file, cb) => {
        //reject file
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
            cb(null, true);

        } else {
            cb(new Error('File Type Not Supported!'), false);
        };
    };

    const Upload = multer({ storage: Storage, limits: {
        fileSize: 1024 * 1024 * 6
    },
        fileFilter: FileFilter
    });

    return Upload;
}