// app.js
// prerequisites
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const connection = require('./db/connection');
const model = require('./db/database');
const Upload = require('./middleware/upload');

// Routes
const controller = require('./routes/routesManager');



// initialize app
const app = express();

// DB Connection
const mongoURL = process.env.MONGO_DB_URL || 'mongodb://localhost/backs-server';

// initialize db connection
connection(mongoURL);

// initialize db schema
const Model = model();

// initialize routes
const Routes = controller(Model);

// initialize requires
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Stacic Files
app.use(express.static('build'));

// handle CORS
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, DELETE, PATCH');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

// USER ROUTES
app.get('/', (req, res) => res.send('Backsberg Stock Application Backend'));
app.get('/allUsers/:username', Routes.AllUsers);
app.post('/login', Routes.LoginUser);
app.post('/addUser', Routes.AddUser);
app.get('/details/:username', Routes.UserDetails);
app.post('/removeUser', Routes.RemoveUser);

// STOCK ROUTES
app.post('/addStock', Upload().single('productImage'), Routes.AddStock);
app.get('/allStock', Routes.AllStock);
app.get('/allLocations', Routes.AllLocations);
app.get('/location/:location', Routes.GetLocation);
app.post('/updateStock', Routes.UpdateStock);
app.get('/bottlesCount', Routes.TotalBottles);


// EXCEL
app.get('/buildExcel', Routes.CreateSpreadsheet);
app.get('/files', Routes.FileList);
app.post('/downloadFile', Routes.DownloadFile);
app.post('/deleteFile', Routes.DeleteFile);

// set port
const PORT = process.env.PORT || 3001;

// start server
const server = app.listen(PORT, () => console.log(`Backend System Running on http://localhost:${PORT} click to open.`));